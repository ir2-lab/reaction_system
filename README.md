# reaction_system.m

Reaction network kinetics in MATLAB  / OCTAVE.

## Description

The ``reaction_system`` MATLAB class aids in the definition and handling of reaction rate equations for complex reaction networks. The equations can be integrated with MATLAB's ODE solvers.

The main features are:
- Simple reaction definition, e.g. : ``'A + B + ... = C + D + ...'``
- Reaction rates based on the law of mass action: $R = k_+ [A][B]... - k_-[C][D]...$
- The forward/backward rate coefficients, $k_{+,-}$, are user defined either as numerical constants or as strings containing valid MATLAB expressions 
- The latter allows implementation of user-defined, parametrized rate laws.

The project has been inspired by the (far more advanced) Julia package [Catalyst.jl](https://catalyst.sciml.ai/dev/).

## Getting started

Start with the [tutorial](./tutorial.ipynb) which describes the basic functionality. 

More elaborate use cases can be found in the ``examples`` folder:
- [robertson.m](./examples/robertson.m): Solution of Robertson reaction system in various ways
- [iron_stage_I_II.m](./examples/iron_stage_I_II.m): Study defect reactions during Stage I & II recovery in irradiated iron
- [iron_100K_irrad](./examples/iron_100K_irrad.m): Defect reactions during Fe irradiation at 100 K

## Installation

The ``reaction_system`` MATLAB class is fully contained in the single file [m/reaction_system.m](./m/reaction_system.m). Copy this file to a folder within the MATLAB path (e.g. your current MATLAB working folder).

## Compatibility

Currently the development is done in OCTAVE, so some compatibility issues may arise in MATLAB.


