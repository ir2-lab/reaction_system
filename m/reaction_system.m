classdef reaction_system < handle
  properties (GetAccess = public, SetAccess = private)
    Reactants = struct (); % Count = n
    Kp = {}; % m x 1, numbers or strings
    Km = {}; % m x 1, numbers or strings
  endproperties

  properties (private)
    Reactions = {};
    idx = {}; % m X 2
    B = []; % n x m
  endproperties


  methods
    function p = reaction_system (a)
      if (nargin == 1)
        if (isa (a, "reaction_system"))
          p.Reactants = containers.Map(a.Reactants);
          p.Reactions = a.Reactions;
          p.idx = a.idx;
          p.B = a.B;
          p.Kp = a.Kp;
          p.Km = a.Km;
        else
          error ("reaction_system: a must be a reaction system");
        end
      end
    end


    % the ode system as function handle
    % varargin is a list of (name, value) pairs, e.g.
    % 'a',1.2,'b',0.3e-5,...
    function h = ode_func(this, varargin)
      dxdt = reaction_system_dxdt(this);
      n = length(dxdt);
      s = '@(t,x)[';
      for i=1:(n-1)
        s = [s sprintf('%s; ',dxdt{i})];
      end
      s = [s sprintf('%s];',dxdt{n})];

      if nargin>1,
        parstr = this.parameter_str(varargin{:});
        if ~isempty(parstr)
          eval(parstr);
        end
      end

      h = str2func(s);
    end

    % return a cell array of strings, each representing the rate of change
    % of a species concentration.
    % Can be used to construct function handles
    function dxdt = reaction_system_dxdt(this)
      R = reaction_rates(this);
      dxdt = {};
      for i=1:size(this.B,1),
        dd = '';
        for j=1:size(this.B,2),
          if this.B(i,j)
            if this.B(i,j)==1
              dd = [dd sprintf('+(%s)',R{j})];
            elseif this.B(i,j)==-1
              dd = [dd sprintf('-(%s)',R{j})];
            else
              dd = [dd sprintf('%+g*(%s)',this.B(i,j),R{j})];
            endif
          endif
        endfor
        if isempty(dd), dd='0'; end
        dxdt = {dxdt{:} dd};
      end
    end

    % reaction rates as strings to be used in a function handle
    function R = reaction_rates(this)
      R = {};
      for i=1:size(this.Reactions,1)
        r = [ ...
        reaction_system.mass_act_str(this.Kp{i},this.idx{i,1},'')  ...
        reaction_system.mass_act_str(this.Km{i},this.idx{i,1},'-')];
        if isempty(r),
          r='0';
        endif
        R = { R{:}, r };
      end
    end

    % add a reaction
    %   reaction : a string like "A + B + ... = C + D + ..."
    %   kp : forward rate, a number or string
    %   km : backward rate, optional
    function this = add_reaction(this,reaction,kp,km)
      % check input
      if nargin<3,
        error('Error: At least the forward rate constant is needed.')
      end
      if nargin<4,
        km = 0;
      end

      if ~( (isreal(kp) && isscalar(kp)) || ischar(kp) )
        error('Error: rate consts are either scalar or strings');
      end

      if ~( (isreal(km) && isscalar(km)) || ischar(km) )
        error('Error: rate consts are either scalar or strings');
      end

      [l,r] = reaction_system.parse_reaction(reaction);

      if isempty(this.Reactions)
        this.Reactions = {l,r};
        % this.Reactants = containers.Map(l{1},1);
        this.Reactants = containers.Map();
        this.Kp = {kp};
        this.Km = {km};
      else
        m = size(this.Reactions,1);
        this.Reactions{m+1,1} = l;
        this.Reactions{m+1,2} = r;
        this.Kp(m+1) = {kp};
        this.Km(m+1) = {km};
      endif

      for i=1:length(l),
        if l{i}~='0' && ~isKey(this.Reactants,l{i})
          this.Reactants(l{i}) = this.Reactants.Count + 1;
        end
      end
      for i=1:length(r),
        if r{i}~='0' && ~isKey(this.Reactants,r{i})
          this.Reactants(r{i}) = this.Reactants.Count + 1;
        end
      end

      update_reactions(this);
    end % add_reaction

    function disp (this)
      [S, R] = reaction_strings(this);
      w = 0;
      nr = length(S);
      for i=1:nr,
        l = length(S{i});
        if l>w, w=l; end
      endfor
      w1 = 1;
      if nr>9, w1=2; end
      fmt = ['(%' num2str(w1) 'd) %-' num2str(w) 's   %s'];
      for i=1:size(this.Reactions,1),
        disp(sprintf(fmt,i,S{i},R{i}))
      endfor

      disp(' ')
      w = reaction_system.max_str_width(this.Reactants.keys);
      for i=1:this.Reactants.Count,
        eqstr = '';
        for j=1:size(this.Reactions,1)
          if this.B(i,j),
            rstr = sprintf('R(%d)',j);
            if abs(this.B(i,j))==1,
              if this.B(i,j)>0,
                eqstr = [ eqstr '+' rstr];
              else
                eqstr = [ eqstr '-' rstr];
              end
            else
              eqstr = [eqstr ...
              sprintf('%+d*',this.B(i,j)) rstr];
            end
          endif
        endfor
        if isempty(eqstr), eqstr='0'; end
        disp([sprintf(['d[%' num2str(w) 's]/dt = '],...
        this.Reactants.keys{i}) eqstr])
      endfor
    end % disp
  end % public methods

  methods (Access=protected)
    % recalculate internal indexes, matrices, etc
    function update_reactions(this)
      m = size(this.Reactions,1);
      n = this.Reactants.Count;

      % fix the species idx
      keys = this.Reactants.keys;
      for i=1:n, this.Reactants(keys{i}) = i; end

      % reaction idx
      idx = cell(m,2);
      for i=1:m,
        r = this.Reactions{i,1};
        k = [];
        for j=1:length(r),
          k = [ k, this.Reactants(r{j}) ];
        end
        idx(i,1) = k;
        r = this.Reactions{i,2};
        k = [];
        for j=1:length(r),
          k = [ k, this.Reactants(r{j}) ];
        end
        idx(i,2) = k;
      end

      % reaction matrix 2
      B = zeros(n,m);
      for i=1:n,
        for j=1:m,
          B(i,j) -= length(find(idx{j,1}==i));
          B(i,j) += length(find(idx{j,2}==i));
        endfor
      end

      this.idx = idx;
      this.B = B;

    end % update reactions

    % return string representation of reactions and rates
    % to be used for display purposes
    function [S, R] = reaction_strings(this)
      S = {}; R = {};
      for i=1:size(this.Reactions,1)
        % reactants
        p = this.Reactions{i,1};
        if isempty(p),
          s = '0';
          r = this.K_to_str(this.Kp{i});
        else
          s = p{1};
          r = '';
          ks = this.K_to_str(this.Kp{i});
          if ~isempty(ks),
            r = sprintf('%s*[%s]',ks,p{1});
          end
          for j=2:length(p),
            s = [s sprintf(' + %s',p{j})];
            if ~isempty(ks), r = [r sprintf('*[%s]',p{j})]; end
          end
        end
        % products
        p = this.Reactions{i,2};
        if isempty(p),
          s = [s ' -> 0'];
        else
          s = [s ' -> ' p{1}];
          ks = this.K_to_str(this.Km{i});
          if ~isempty(ks),
            r = [r ' - ' sprintf('%s*[%s]',ks,p{1})];
          end
          for j=2:length(p),
            s = [s sprintf(' + %s',p{j})];
            if ~isempty(ks), r = [r sprintf('*[%s]',p{j})]; end
          end
        end
        S = { S{:}, s };
        if isempty(r),
          r='R= 0';
        else
          r = ['R= ' r];
        endif
        R = { R{:}, r };
      end
    end

  end % protected methods

  methods (Static = true)

    % varargin is a list of (name,value) pairs,
    % e.g., 'a',0.1,'b',1e-5,...
    % this function returns a string that can be evaluated
    % and instantiate these parameters, e.g.
    % str = 'a=0.1;b=1e-5;'
    function str = parameter_str(varargin)
      str = '';
      l = length(varargin);
      n = floor(l/2);
      for i = 1:n,
        var_name = varargin{2*(i-1) + 1};
        var_val  = varargin{2*(i-1) + 2};
        if ~ischar(var_name)
          error("var name not string");
        end
        if ~isnumeric(var_val)
          error("var val not numeric");
        end
        % put the variables in the workspace
        str = [ str var_name '=' num2str(var_val,15) ';'];
      end
    end


  end % static public methods

  methods (Static = true, Access = private)

    % find the max width in the cell string array s
    function w = max_str_width(s)
      w = 0;
      for i=1:length(s),
        l = length(s{i});
        if l>w, w=l; end
      end
    end

    % parse the reaction string "A + B + ... = C + D + ..."
    % return l={'A','B',...}, r={'C','D',...}
    function [l,r] = parse_reaction(reaction)

      if ~ischar(reaction),
        error('Error parsing reaction: it is not char type.',reaction);
      end

      k = strfind(reaction,'=');
      if isempty(k),
        error('Error parsing reaction: %s. = sign not found.',reaction);
      end

      [leftp,rightp] = strtok(reaction,'=');
      leftp = strtrim(leftp);
      if isempty(leftp),
        error('Error parsing reaction: %s. Left side is empty.',reaction);
      end

      rightp = strtrim(rightp(2:end));
      if isempty(rightp),
        error('Error parsing reaction: %s. Right side is empty.',reaction);
      end

      l = reaction_system.parse_part(leftp);
      r = reaction_system.parse_part(rightp);

      if isempty(l) && isempty(r),
        error('Error parsing reaction: %s. 0 = 0',reaction);
      end
    end

    % parse the left or right part of A + B + ... = C + D + ...
    % return cell arrat of reactants, e.g., {'A', 'B', ... }
    function q = parse_part(p)
      q = {};
      r = p;
      while ~isempty(r)
        [tok,r] = strtok(r,'+');
        tok = strtrim(tok);
        if isempty(tok),
          error('Error parsing %s: reactant not found.',p);
        end
        if tok~='0', q = {q{:} tok}; end
        r = r(2:end); % get rid of '+'
      end
    end

    % produce a string like k*x(1)*x(2)*... representing
    % a mass action rate law
    function r = mass_act_str(k,i,pre)
      r = '';
      if ischar(k),
        r = [r sprintf('(%s)',k)];
        if ~isempty(i)
          for j=1:length(i)
            r = [r sprintf('*x(%d)',i(j))];
          end
        end
      else
        if k~=0,
          r = [r sprintf('%g',k)];
          if ~isempty(i)
            for j=1:length(i)
              r = [r sprintf('*x(%d)',i(j))];
            end
          end
        end
      end
      if ~isempty(r),
        r = [pre r];
      end
    end

    function r = K_to_str(k)
      r = '';
      if ischar(k)
        r = sprintf('(%s)',k);
      else
        if k~=0,
          r = sprintf('%g',k);
        end
      end
    end

  end % static protected methods

  endclassdef
