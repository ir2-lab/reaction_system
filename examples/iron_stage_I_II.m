%% Simulate Stage I recovery of irradiated Fe 
clear
addpath('../m')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defects = 
%   I - Self interstitial atoms (SIA) 
%   V - vacancies
%   I2,I3,I4 - SIA clusters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Constants
kb = 8.617e-5; %boltzmann constant [eV/K]

% Fe lattice data
afe = 0.286; % afe [nm]
Vat = afe.^3./2; % Atomic volume [nm^3]
rat = (3*Vat/4/pi).^(1/3); % atomic radius [nm]

% isochronal annealing 
Ta = 80:2:200; % [K]
dta = 120; % [s]

% SIA diffusion in Fe
D0 = 3*3.3*1e13*(afe/rat)^3; % Pre-exp. diffusion const [1/s] 
Em = 0.34; % migration energy of SIA [eV] 
Di = D0*exp(-Em/kb./Ta); 
% I2 diffusion in Fe
Em = 0.42; % migration energy of I2, I3 [eV] 
Dii = D0*exp(-Em/kb./Ta);


R = reaction_system();

% recombination
add_reaction(R, 'V + I = 0',  'k1');
add_reaction(R, 'V + I2 = I', 'k2');
add_reaction(R, 'V + I3 = I2','k2');

% clustering
add_reaction(R, 'I + I  = I2', 'k1');
add_reaction(R, 'I + I2 = I3', 'k3');
add_reaction(R, 'I + I3 = I4', 'k3');

disp(R)

% initial condition
nr = R.Reactants.Count;
x0 = zeros(nr,1);
i = R.Reactants('V'); x0(i) = 100e-6;
i = R.Reactants('I'); x0(i) = 100e-6;

% Allocate variable to hold solution
X = zeros(length(Ta),nr);

% solve
disp(' ')
fprintf(stdout,'Performing integration ');
fflush(stdout);
options = odeset('Mass',eye(nr),...
    'RelTol',1e-6,...
    'AbsTol',1e-12);
tic
% 1st T point
i = 1;
fh = R.ode_func('k1',Di(i),'k2',Dii(i),'k3',Di(i)+Dii(i));
[t,x] = ode15s(fh,[0 dta],x0,options);
X(1,:) = x(end,:);
% continue    
for i=2:length(Ta),
  x0 = X(i-1,:)';
  fh = R.ode_func('k1',Di(i),'k2',Dii(i),'k3',Di(i)+Dii(i));
  [t,x] = ode15s(fh,[0 dta],x0,options); 
  X(i,:) = x(end,:);
  if mod(i,10)==0,
    fprintf(stdout,'.');
    fflush(stdout);
  end
end
fprintf(stdout,'\n');
fflush(stdout);
toc

disp('Plot results')
figure 1
clf
plot(Ta,X) 
lbls = R.Reactants.keys;
legend(lbls)
xlabel('T (K)')
ylabel('dpa')
title('Stage I+II in irradiated Fe - defect concentrations')
figure 2
clf
i = R.Reactants('V');
plot(Ta(2:end),-diff(X(:,i))./diff(Ta)/X(1,i)) 
ylabel('-X_0^{-1} dX/dT  (1/K)')
xlabel('T (K)')
title('Stage I+II in irradiated Fe - recovery rate')


