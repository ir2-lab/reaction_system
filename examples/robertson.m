clear
addpath('../m')

%% 1. use of reaction_system to solve Robertson's problem
R = reaction_system();
add_reaction(R, 'A = B', 0.04);
add_reaction(R, 'B + B  = B + C ', 3e7);
add_reaction(R, 'B + C  = A + C ', 1e4);

disp(R)

nr = R.Reactants.Count;
options = odeset('Mass',eye(nr),...
    'RelTol',1e-6,...
    'AbsTol',1e-12,...
    'Stats','on'); 
    
tspan = [0 logspace(-6,8,120)];

disp(' ')
disp('Started reaction_system + ode15s ...')
tic  
[t,x] = ode15s(ode_func(R),tspan,[1 0 0]',options); 
toc

figure 1
clf
semilogx(t(2:end),x(2:end,:).*[1 10000 1],'.-')
legend(R.Reactants.keys)
title('Robertson DAE problem solved by reaction_system + ode15s');
xlabel('t')
ylabel('x(t)');
drawnow

%% 2. Standard solution with ode15s
%% - shows that  reaction_system() does not bring any calc. overhead

function r = robertson_plain (t, y)
  r = [ -0.04*y(1) + 1e4*y(2)*y(3)
         0.04*y(1) - 1e4*y(2)*y(3) - 3e7*y(2)^2
         3e7*y(2)^2 ];
endfunction

disp(' ')
disp('Started plain ode15s ...')
tic  
[t,x] = ode15s(@robertson_plain,tspan,[1 0 0]',options); 
toc

%% 3. Solve as a DAE with ode15s
%% - utilize mass balance eq. [A]+[B]+[C]=1
%% - 50-fold speed-up

function r = robertson_dae (t, y)
  r = [ -0.04*y(1) + 1e4*y(2)*y(3)
         0.04*y(1) - 1e4*y(2)*y(3) - 3e7*y(2)^2
         y(1) + y(2) + y(3) - 1 ];
endfunction

options = odeset('Mass',[1 0 0; 0 1 0; 0 0 0],...
    "MStateDependence", "none", ...
    'RelTol',1e-6,...
    'AbsTol',1e-12,...
    'Stats','on'); 

disp(' ')
disp('Started ode15s ...')
tic  
[t,y] = ode15s(@robertson_dae, tspan, [1 0 0]', options);
toc

figure 2
y(:,2) *= 1e4;
semilogx(t(2:end),y(2:end,:),'.-');
xlabel('t')
ylabel('y(t)');
title('Robertson DAE problem with mass balance, solved by ode15s');
legend('y_1','10^4 y_2','y_3')