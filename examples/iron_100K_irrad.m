%% Simulate irradiated of Fe at 100 K 
clear
addpath('../m')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defects = 
%   I - Self interstitial atoms (SIA) 
%   V - vacancies
%   I2,I3,I4 - SIA clusters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Constants
kb = 8.617e-5; %boltzmann constant [eV/K]

% Fe lattice data
afe = 0.286; % afe [nm]
Vat = afe.^3./2; % Atomic volume [nm^3]
rat = (3*Vat/4/pi).^(1/3); % atomic radius [nm]

% SIA diffusion in Fe
D0 = 3*3.3*1e13*(afe/rat)^3; % Pre-exp. diffusion const [1/s] 
Em = 0.34; % migration energy of SIA [eV] 
Di = @(T) D0*exp(-Em/kb./T); 
% I2 diffusion in Fe
Em = 0.42; % migration energy of I2, I3 [eV] 
Dii = @(T) D0*exp(-Em/kb./T);

% defect (I-V pairs) generation
G = 4e-8; % [s^-1]

R = reaction_system();

% defect generation
add_reaction(R, '0 = V + I', G);

% recombination
add_reaction(R, 'V + I = 0',  'k1');
add_reaction(R, 'V + I2 = I', 'k2');
add_reaction(R, 'V + I3 = I2','k2');

% clustering
add_reaction(R, 'I + I  = I2', 'k1');
add_reaction(R, 'I + I2 = I3', 'k3');
add_reaction(R, 'I + I3 = I4', 'k3');

disp(R)

% solve for T=100K, 0 <= t <= 0.01/G
disp(' ')
disp('Performing integration ');

tic
nr = R.Reactants.Count;
options = odeset('Mass',eye(nr),...
    'RelTol',1e-6,...
    'AbsTol',1e-12);
T = 100;
fh = R.ode_func('k1',Di(T),'k2',Dii(T),'k3',Di(T)+Dii(T));
t = [0 logspace(0,log10(0.01/G),51)];
%t = [0 0.01/G];
[t,x] = ode15s(fh,t,zeros(nr,1),options);
toc

disp('Plot results')
figure 1
clf
plot(G*t,x)
lbls = R.Reactants.keys;
legend(lbls)
xlabel('dpa')
ylabel('dpa')
title(['T(K) = ' num2str(T)])



